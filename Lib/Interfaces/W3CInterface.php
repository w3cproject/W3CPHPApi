<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 27.02.2017
 * Time: 19:04
 */
namespace W3C\Lib\Interfaces;

/**
 * Interface W3CInterface
 * @package Interfaces
 */
interface W3CInterface {

    /**
     * @return mixed
     */
    public function getAuthor();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getVersion();
}