<?

namespace W3C\Lib\Components;

/**
 * Class CurlHelper
 * @package W3C\Lib\Components
 */
class CurlHelper extends CurlAbstract {

    /**
     * @param string $url
     * @param string $login
     * @param string $password
     * @return string
     */
    public function getNTLMData(string $url, string $login, string $password) {
        $ch = \curl_init();

        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_NTLM);
        \curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");

        \ob_start();

        $result = \curl_exec($ch);

        $content = \ob_get_contents();

        \ob_clean();

        return $content;
    }
}