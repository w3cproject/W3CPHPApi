<?

namespace W3C\Lib\Components;


use Curl\Curl;
use W3C\Lib\Components\Contracts\CurlContract;

/**
 * Class CurlAbstract
 * @package W3C\Lib\Components
 */
abstract class CurlAbstract implements CurlContract{

    /**
     * @var Curl
     */
    public static $curl;

    public function __construct() {
        self::$curl = $this->getCurl();
    }

    /**
     * @return Curl
     */
    public function getCurl(): Curl {
        return new Curl();
    }
}