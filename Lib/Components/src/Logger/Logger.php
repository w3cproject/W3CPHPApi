<?

namespace W3C\Lib\Components;

/**
 * Class Logger
 * @package W3C\Lib\Components
 */
class Logger extends LoggerAbstract {

    public function __construct(string $folderPath, string $fileName, $fileMode = '0755', string $fileExt = 'log') {
        $this->setFolderPath($folderPath);
        $this->setFileMode($fileMode);
        $this->setFileExt($fileExt);
        $this->setFileName($fileName);
    }

    public function add($element, $logLevel = null) {
        if (\is_object($element)) {
            $element = \get_class($element);
        }

        $logLevel = (\is_null($logLevel))
            ? $this->getSystemLevel()
            : $logLevel;

        if (!\in_array($element, $this->logger)) {
            $this->logger[] = [
                'time'      => \date($this->getDateFormat(), \strtotime('now')),
                'target'    => (string)$element,
                'log_level' => $logLevel
            ];
        }
    }

    public function write(bool $asArray = false) {
        if (!$this->folderPathExists()) {
            \mkdir($this->getFolderPath(), $this->getFileMode(), true);
        }

        foreach ($this->logger as $log) {
            $time     = $log['time'];
            $target   = $log['target'];
            $logLevel = $log['log_level'];

            $data = "[ $time ][ $logLevel ]: $target" . PHP_EOL;

            if ($asArray) {
                $data = "[ $time ][ $logLevel ]: " . var_export($log, true) . PHP_EOL;
            }

            \file_put_contents($this->getFullPath(),
                               $data,
                               FILE_APPEND);
        }

        $this->clear();
    }

    public function clear() {
        $this->logger = [];
    }

    /**
     * Clear *.log file
     */
    public function clearFile() {
        \file_put_contents($this->getFullPath(), '');
    }

    /**
     * @return int
     */
    public function getSize() {
        return \filesize($this->getFullPath());
    }

    public function getResponse(): array {
        return $this->logger;
    }
}