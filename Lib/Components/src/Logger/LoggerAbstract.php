<?

namespace W3C\Lib\Components;

use W3C\Lib\Components\Contracts\LoggerContract;

abstract class LoggerAbstract implements LoggerContract {

    /**
     * system levels types
     */
    const SYSTEM_LEVEL_WARN = 'WARN',
        SYSTEM_LEVEL_INFO = 'INFO',
        SYSTEM_LEVEL_ERROR = 'ERROR',
        SYSTEM_LEVEL_MESSAGE = 'MESSAGE';

    /**
     * @var array $logger
     * @var string $dateFormat
     * @var string $folderPath
     * @var string $fileName
     * @var string $fileMode
     * @var string $systemLevel
     */
    protected $logger = [],
        $dateFormat = 'd.m.Y H:i:s',
        $folderPath,
        $fileName,
        $fileExt,
        $fileMode,
        $systemLevel = self::SYSTEM_LEVEL_INFO;

    /**
     * @param $level
     */
    public function setSystemLevel($level) {
        $this->systemLevel = $level;
    }

    /**
     * @return string
     */
    public function getSystemLevel() {
        return $this->systemLevel;
    }

    /**
     * @param string $fileMode
     */
    public function setFileMode($fileMode) {
        $this->fileMode = $fileMode;
    }

    /**
     * @return string
     */
    public function getFileMode() {
        return $this->fileMode;
    }

    /**
     * @param string $fileExt
     */
    public function setFileExt($fileExt) {
        $this->fileExt = $fileExt;
    }

    /**
     * @return string
     */
    public function getFileExt() {
        return $this->fileExt;
    }

    /**
     * @param string $format
     */
    public function setDateFormat(string $format) {
        $this->dateFormat = $format;
    }

    /**
     * @return string
     */
    public function getDateFormat(): string {
        return $this->dateFormat;
    }

    /**
     * @param string $folderPath
     */
    public function setFolderPath(string $folderPath) {
        $this->folderPath = $folderPath;
    }

    /**
     * @return string
     */
    public function getFolderPath(): string {
        return $this->folderPath;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName) {
        $this->fileName = $fileName . '.' . $this->getFileExt();
    }

    /**
     * @return string
     */
    public function getFileName(): string {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getFullPath() {
        return $this->getFolderPath() . $this->getFileName();
    }

    /**
     * @return bool
     */
    public function folderPathExists() {
        return \file_exists($this->getFolderPath());
    }
}