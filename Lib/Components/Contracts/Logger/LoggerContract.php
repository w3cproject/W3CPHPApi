<?

namespace W3C\Lib\Components\Contracts;

/**
 * Interface LoggerContract
 * @package W3C\Lib\Components\Contracts\Logger
 */
interface LoggerContract {

    /**
     * LoggerContract constructor.
     * @param string $folderPath
     * @param string $fileName
     * @param $fileMode
     * @param string $fileExt
     */
    public function __construct(string $folderPath, string $fileName, $fileMode, string $fileExt);

    /**
     * Add element to logger array
     *
     * @param $element
     * @param null $logLevel
     */
    public function add($element, $logLevel = null);

    /**
     * Write all logs into file
     *
     * @param bool $asArray
     */
    public function write(bool $asArray);

    /**
     * Clear logger array
     */
    public function clear();

    /**
     * Return logger array
     *
     * @return array
     */
    public function getResponse(): array;
}