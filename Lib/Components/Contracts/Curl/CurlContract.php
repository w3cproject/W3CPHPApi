<?

namespace W3C\Lib\Components\Contracts;

use Curl\Curl;

/**
 * Interface CurlContract
 * @package W3C\Lib\Components\Contracts\Curl
 */
interface CurlContract {

    /**
     * @return Curl
     */
    public function getCurl(): Curl;
}