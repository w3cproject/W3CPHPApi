<?
declare(strict_types=1);

namespace W3C\Bitrix;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Application;
use CIBlockElement;

/**
 * Class Element
 * @package W3C\Bitrix
 */
class Element {
    /**
     * @param int $id
     *
     * @return int
     */
    public static function emptinessOfAnIblock(int $id): int {
        $element = \Bitrix\Iblock\ElementTable::getRow(
            [
                'select' => [
                    'ID'
                ],
                'filter' => [
                    '=IBLOCK_ID' => (int)$id
                ]
            ]
        );

        return \count($element);
    }

    /**
     * @param array $filter
     *
     * @return int
     */
    public static function countElementTable(array $filter = []): int {
        $elements = ElementTable::getList(
            [
                'select' => [
                    'ID'
                ],
                'filter' => $filter
            ]
        )->fetchAll();

        return \count($elements);
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public static function getElementPropertiesByID(int $id): array {
        $res = CIBlockElement::GetByID($id)->GetNextElement();

        $props = $res->GetProperties();

        return $props;
    }

    /**
     * @param string $query
     *
     * @return array|false
     */
    public function executeQueryString(string $query) {
        $db = Application::getConnection();

        return $db->query($query)->fetch();
    }
}