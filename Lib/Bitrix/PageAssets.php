<?

namespace W3C\Bitrix;

use Bitrix\Main\Page\Asset;

/**
 * Class PageAssets
 * @package W3C\Bitrix
 */
class PageAssets {
    /**
     * @param iterable $styles
     * Connect array of styles
     */
    public static function addStyles(iterable $styles) {
        foreach ($styles as $style) {
            Asset::getInstance()->addCss($style);
        }
    }

    /**
     * @param $scripts
     * Connect array of scripts
     */
    public static function addScripts(iterable $scripts) {
        foreach ($scripts as $script) {
            Asset::getInstance()->addJs($script);
        }
    }
}