<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 07.07.2017
 * Time: 11:42
 */

namespace W3C\Bitrix;

use CFile;
use CUser;

/**
 * Class Image
 * @package W3C\Bitrix
 */
class Image {
    /**
     * @param int $id
     *
     * @return bool
     */
    public static function getImageUrlFromID(int $id) {
        if (is_numeric($id)) {
            $imageArray = \CFile::GetFileArray($id);

            return $imageArray['SRC'];
        }

        return false;
    }

    /**
     * @param int $id
     * @param int $width
     * @param int $height
     * @param     $type
     *
     * @return bool
     */
    public static function resizeImageByID(
        int $id,
        int $width,
        int $height,
        $type = BX_RESIZE_IMAGE_PROPORTIONAL
    ) {

        if (is_numeric($id)) {
            $img = \CFile::ResizeImageGet(
                (int)$id,
                [
                    'width'  => (int)$width,
                    'height' => (int)$height
                ],
                $type,
                false
            );

            return $img['src'];
        }

        return false;
    }

    /**
     * @param array $photo
     */
    public static function updatePhoto(array $photo) {
        global $USER;

        $user = CUser::GetByID($USER->GetID())->GetNext();

        $photo['del']      = 'Y';
        $photo['old_file'] = (!empty($user['PERSONAL_PHOTO'])) ? $user['PERSONAL_PHOTO'] : '';

        (new CUser())->Update(
            $USER->GetID(),
            [
                'PERSONAL_PHOTO' => $photo
            ]
        );
    }

    public static function removePhoto() {
        global $USER;

        $user = CUser::GetByID($USER->GetID())->GetNext();

        $arPhoto = CFile::GetFileArray($user['PERSONAL_PHOTO']);

        CFile::Delete($arPhoto['ID']);
    }
}