<?

namespace W3C\Bitrix;

class FilesLogger {
    /**
     * Files errors logger
     * @var array
     */
    public static $filesLogger = [];

    /**
     * Add failed required element to logger array
     *
     * @param $element
     */
    public static function add($element) {
        if (!\in_array($element, self::$filesLogger)) {
            self::$filesLogger[] = $element;
        }
    }

    /**
     * clear logger
     */
    public static function clear() {
        self::$filesLogger = [];
    }
}