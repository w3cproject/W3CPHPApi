<?

namespace W3C\Bitrix;

/**
 * Class File
 * @package W3C\Bitrix
 */
class File extends \CFile {

    /**
     * Returns formatted float|null file size from kb to mb
     *
     * @param     $size
     * @param int $precision
     *
     * @return float|null
     */
    public static function getFileFormattedSize($size, int $precision = 2): ?float {
        return \round(self::FormatSize((int)$size, $precision));
    }
}