<?

namespace W3C\Bitrix\HL;

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity\DataManager;

/**
 * Class DataException
 * @package W3C\Bitrix\HL
 */
class DataException extends \TypeError {
}

/**
 * Class Data
 * @package W3C\Bitrix\HL
 */
class Data {
    /**
     * @param       $tableObj DataManager
     * @param array $select
     * @param array $filter
     * @param int   $limit
     * @param array $order
     *
     * @return mixed
     * @throws DataException
     */
    public static function getTableList(
        $tableObj,
        $select = ['*'],
        $filter = [],
        $limit = 10,
        $order = []
    ) {
        if (!is_object($tableObj)) {
            throw new DataException('not a Object, use: "new TableName()"');
        }

        $rsData = $tableObj::getList(
            [
                'select' => $select,
                'filter' => $filter,
                'limit'  => $limit,
                'order'  => $order
            ]
        )->fetchAll();

        return $rsData;
    }

    /**
     * @param int $id
     *
     * @return DataManager
     */
    public static function getDataClassByID(int $id): DataManager {
        $hlBlock = HL\HighloadBlockTable::getById((int)$id)->fetch();
        $entity  = HL\HighloadBlockTable::compileEntity($hlBlock);

        return $entity->getDataClass();
    }
}