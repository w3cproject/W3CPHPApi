<?

namespace W3C\Bitrix;

/**
 * Class Includes
 * @package W3C\Bitrix
 */
class Includes extends FilesLogger {

    /**
     * paths to /bitrix/modules/main/include/ usable files
     * for quick require
     */
    const INCLUDES = [
        'prolog'        => '/bitrix/modules/main/include/prolog.php',
        'prolog_before' => '/bitrix/modules/main/include/prolog_before.php',
        'prolog_after'  => '/bitrix/modules/main/include/prolog_after.php',
        'epilog'        => '/bitrix/modules/main/include/epilog.php',
        'epilog_before' => '/bitrix/modules/main/include/epilog_before.php',
        'epilog_after'  => '/bitrix/modules/main/include/epilog_after.php',
        'urlrewrite'    => '/bitrix/modules/main/include/urlrewrite.php',
    ];

    /**
     * standard constants file name
     */
    const CONSTANTS_FILE_NAME = '_constants';

    /**
     * $filePath - file path without document root!
     *
     * Require file if exists
     *
     * @param string $filePath
     */
    public static function includeFile(string $filePath) {
        $file = $_SERVER["DOCUMENT_ROOT"] . $filePath;

        if (\is_readable($file)) {
            require_once $file;
        }
    }

    /**
     * Require prolog.php
     *
     * @return void
     */
    public static function getProlog() {
        self::includeFile(self::INCLUDES['prolog']);
    }

    /**
     * Require prolog_before.php
     *
     * @return void
     */
    public static function getPrologBefore() {
        self::includeFile(self::INCLUDES['prolog_before']);
    }

    /**
     * Require prolog_after.php
     *
     * @return void
     */
    public static function getPrologAfter() {
        self::includeFile(self::INCLUDES['prolog_after']);
    }

    /**
     * Require epilog.php
     *
     * @return void
     */
    public static function getEpilog() {
        self::includeFile(self::INCLUDES['epilog']);
    }

    /**
     * Require epilog_before.php
     *
     * @return void
     */
    public static function getEpilogBefore() {
        self::includeFile(self::INCLUDES['epilog_before']);
    }

    /**
     * Require epilog_after.php
     *
     * @return void
     */
    public static function getEpilogAfter() {
        self::includeFile(self::INCLUDES['epilog_after']);
    }

    /**
     * Require urlrewrite.php
     *
     * @return void
     */
    public static function getUrlRewrite() {
        self::includeFile(self::INCLUDES['urlrewrite']);
    }

    /**
     * Required constants files by language
     *
     * @param string $lang
     * @param string $dir
     * @param string $fileName
     */
    public static function multipleConstantsRequire(string $lang = LANGUAGE_ID, string $dir = __DIR__,
                                                    string $fileName = self::CONSTANTS_FILE_NAME) {

        $constantsFile = $dir . $fileName . ".$lang.php";

        if (\is_readable($constantsFile)) {
            require_once $constantsFile;
        } else {
            FilesLogger::add($constantsFile);
        }
    }

    /**
     * @param $mainDir
     * @param array $files
     */
    public static function multipleFilesRequire($mainDir, array $files) {
        foreach ($files as $file) {
            $file = $mainDir . $file;

            if (\is_readable($file)) {
                require_once $file;
            } else {
                FilesLogger::add($file);
            }
        }
    }
}