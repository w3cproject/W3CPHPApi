<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 07.07.2017
 * Time: 16:14
 */

namespace W3C\Bitrix;

use Bitrix\Main\UserGroupTable;

class Group {
    /**
     * @param int $groupID
     * @param int $userID
     */
    public static function addToGroup(int $groupID, int $userID) {
        UserGroupTable::add([
            'GROUP_ID' => $groupID,
            'USER_ID'  => $userID
        ]);
    }

    /**
     * @param int $groupID
     * @param int $userID
     */
    public static function deleteFromGroup(int $groupID, int $userID) {
        UserGroupTable::delete([
            'GROUP_ID' => $groupID,
            'USER_ID'  => $userID
        ]);
    }
}