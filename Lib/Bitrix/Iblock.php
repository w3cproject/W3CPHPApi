<?
declare(strict_types=1);

namespace W3C\Bitrix;

/**
 * Class IblockException
 * @package W3C\Bitrix
 */
class IblockException extends \TypeError {
}

/**
 * Class Iblock
 * @package W3C\Bitrix
 */
class Iblock {

    /**
     * @param $id
     * @param $filter
     *
     * @return array
     * @throws IblockException
     * return array ID and NAME of property
     */
    public static function getPropertiesListByID(int $id, array $filter = []) {
        if (!is_numeric($id)) {
            throw new IblockException('Property id must be integer!');
        }

        $props = [];

        $rsBlock = \CIBlockProperty::GetPropertyEnum(
            (int)$id,
            false,
            $filter
        );

        while ($prop = $rsBlock->GetNext()) {
            $props[] = [
                'ID'   => $prop['ID'],
                'NAME' => $prop['VALUE']
            ];
        }

        return $props;
    }
}