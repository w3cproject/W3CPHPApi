<?

namespace W3C\Lib\Classes;

/**
 * Class PDOConnect
 * @package W3C\Lib\Classes
 */
class PDOConnect {
    /**
     * @var null|\PDO
     */
    private $PDODataBase = null;

    /**
     * @var array
     */
    private $queryResult = [];

    /**
     * @param string $host
     * @param string $name
     * @param string $user
     * @param string $password
     */
    public function connectPDODataBase(
        string $host,
        string $name,
        string $user,
        string $password
    ) {
        try {
            $this->PDODataBase = new \PDO("$host;dbname=$name", $user, $password);
        } catch (\PDOException $e) {
            \print_r($e->getMessage());
            die();
        }
    }

    /**
     * @param string      $where
     * @param string      $from
     * @param string|NULL $field
     *
     * @return array
     */
    public function selectRows(string $where, string $from, string $field = null): array {
        $query = $this->PDODataBase;

        foreach ($query->query("SELECT $where FROM $from") as $row) {
            if ($field !== null) {
                $this->queryResult[] = $row[$field];
            } else {
                $this->queryResult[] = $row;
            }
        }

        $query             = null;
        $this->PDODataBase = null;

        return $this->queryResult;
    }
}