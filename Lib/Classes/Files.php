<?

namespace W3C\Lib\Classes;

/**
 * Class Files
 * @package Classes
 */
class Files {
    /**
     * @param string $path
     *
     * @return string
     */
    public function getJsonContent(string $path): string {
        $file = \file_get_contents($path);

        return \json_encode($file, true);
    }

    /**
     * Returns file extension by name
     * Returns null if string without extension
     *
     * @param string $name
     *
     * @return null|string
     */
    public static function getFileExtension($name): ?string {
        return \pathinfo($name)['extension'];
    }
}