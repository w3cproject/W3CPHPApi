<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 02.06.2017
 * Time: 18:51
 */

namespace W3C\Lib\Classes;

/**
 * Class Security
 * @package W3C\Lib\Classes
 */
class Security {
    public function generateToken($data): string {
        return \hash('sha512', $data);
    }
}