<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 04.07.2017
 * Time: 15:28
 */

namespace W3C\Lib\Classes;

/**
 * Class Headers
 * @package W3C\Lib\Classes
 */
class Headers {
    /**
     * @param array $message
     * @param null  $options
     */
    public static function forceJsonHeaderMessage(array $message, $options = null) {
        \header('Content-type:application/json');

        echo \json_encode($message, $options);
    }
}