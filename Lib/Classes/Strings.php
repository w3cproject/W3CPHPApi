<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 27.02.2017
 * Time: 17:16
 */

namespace W3C\Lib\Classes;

/**
 * Class Strings
 * @package Classes
 */
class Strings {
    /**
     * @var null
     */
    private $string = null;

    /**
     * @param int $length
     *
     * @return null|string
     */
    public function generateString(int $length = 6): string {
        $characters = \array_merge(\range('A', 'Z'), \range('a', 'z'), \range('0', '9'));
        $max        = \count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand         = \mt_rand(0, $max);
            $this->string .= $characters[$rand];
        }

        return $this->string;
    }

    /**
     * @param $str
     *
     * @return string
     * return lowercase string with upper first letter
     */
    public function strToLowerWithFirstUpper($str): string {
        return \strtoupper(\substr($str, 0, 1)) . \strtolower(\substr($str, 1));
    }

    /**
     * Returns declined word
     * how to use:
     * <ul>
     * <li>declineWord(5, ['лопат', 'лопата', 'лопаты']); => лопат</li>
     * <li>declineWord(1, ['лопат', 'лопата', 'лопаты']); => лопата</li>
     * <li>declineWord(2, ['лопат', 'лопата', 'лопаты']); => лопаты</li>
     * </ul>
     * where:
     * <p>$num - count of elements</p>
     * <p>$arr - list of words</p>
     *
     * @param int   $num
     * @param array $arr
     *
     * @return string
     */
    public static function declineWord(int $num, array $arr): string {
        $num = (int)$num;

        $output = null;

        $last = $num % 10;

        if (($num > 4 && $num < 21)) {
            $output .= $arr[0];
        } elseif (($num > 20 || $num < 10) && $last >= 2 && $last <= 4) {
            $output .= $arr[2];
        } elseif ($last == 1 && $num != 11) {
            $output .= $arr[1];
        } else {
            $output .= $arr[0];
        }

        return $output;
    }

    /**
     * @param string $pattern
     * @param array  $params
     *
     * @return string
     */
    public function replaceParamsInPattern(string $pattern, array $params): string {
        foreach ($params as $key => $value) {
            $pattern = \str_replace('{' . $key . '}', $value, $pattern);
        }

        return $pattern;
    }
}