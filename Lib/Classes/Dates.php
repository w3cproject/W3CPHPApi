<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 27.02.2017
 * Time: 17:15
 */

namespace W3C\Lib\Classes;

/**
 * Class Dates
 * @package Classes
 */
class Dates {
    /**
     * @param string $date
     *
     * @return string
     */
    public function formatDate(string $date): string {
        return \strftime("%d, %B, %Y", $date);
    }

    /**
     * @return false|string
     */
    public function getCurrentUTCDate(): string {
        \date_default_timezone_set('UTC');

        return \date('d.m.Y H:i:s');
    }

    /**
     * @param string $first
     * @param string $second
     *
     * @return string
     */
    public function differenceDate(string $first, string $second): string {
        $date1 = new \DateTime($first);
        $date2 = new \DateTime($second);

        $interval = $date2->diff($date1);

        return "Difference : " . $interval->y . " years, " . $interval->m . " months, " . $interval->d . " days ";
    }

    public function differenceDateDays($first, $second) {
        $date1 = new \DateTime($first);
        $date2 = new \DateTime($second);

        $interval = $date2->diff($date1);

        return $interval->d;
    }

    public function getDateByTimeZone(
        string $dateString = 'now',
        string $timezone = 'Europe/Moscow',
        string $patternInput = 'd-m-Y H:i:s',
        string $patternOutput = 'Y-m-d H:i:s'
    ): string {

        $datetime   = new DateTime(\date($patternInput, \strtotime($dateString)));
        $timeEurope = new DateTimeZone($timezone);
        $datetime->setTimezone($timeEurope);

        return $datetime->format($patternOutput);
    }

    public function getRuMonthFromEnglish(
        $dateString,
        array $ruMonths = [],
        array $enMonths = [],
        string $dateFormatOutput = 'j F Y'
    ): string {

        $ruMonths = (\count($ruMonths) > 0)
            ? $ruMonths
            : [
                'Января',
                'Февраля',
                'Марта',
                'Апреля',
                'Мая',
                'Июня',
                'Июля',
                'Августа',
                'Сентября',
                'Октября',
                'Ноября',
                'Декабря'
            ];

        $enMonths = (\count($enMonths) > 0)
            ? $enMonths
            : [
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ];

        $ruDate = \str_replace($enMonths, $ruMonths, \date($dateFormatOutput, $dateString));

        return $ruDate;
    }
}