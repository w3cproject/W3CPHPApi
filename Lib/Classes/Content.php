<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 27.02.2017
 * Time: 17:14
 */

namespace W3C\Lib\Classes;

/**
 * Class Content
 * @package Classes
 */
class Content {
    /**
     * @param mixed $content
     */
    public function pre($content) {
        echo '<pre>';
        \print_r($content);
        echo '</pre>';
    }

    /**
     * @param string $fileName
     * @param        $var
     * @param int    $method
     *
     * @return int
     */
    public function put(string $fileName, $var, $method = FILE_APPEND) {
        return \file_put_contents(
            $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $fileName,
            \var_export($var, true),
            $method
        );
    }

    /**
     * @param string $str
     *
     * @return string
     */
    public function transliteration(string $str): string {
        // транслитерация корректно работает на страницах с любой кодировкой
        // ISO 9-95
        static $tbl = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ж' => 'g',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'ы' => 'y',
            'э' => 'e',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ж' => 'G',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'Y',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Ы' => 'Y',
            'Э' => 'E',
            'ё' => "yo",
            'х' => "h",
            'ц' => "ts",
            'ч' => "ch",
            'ш' => "sh",
            'щ' => "shch",
            'ъ' => "",
            'ь' => "",
            'ю' => "yu",
            'я' => "ya",
            'Ё' => "YO",
            'Х' => "H",
            'Ц' => "TS",
            'Ч' => "CH",
            'Ш' => "SH",
            'Щ' => "SHCH",
            'Ъ' => "",
            'Ь' => "",
            'Ю' => "YU",
            'Я' => "YA",
            ' ' => "_",
            '№' => "",
            '«' => "<",
            '»' => ">",
            '—' => "-"
        ];

        return \strtr($str, $tbl);
    }
}