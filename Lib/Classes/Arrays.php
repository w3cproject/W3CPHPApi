<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 04.07.2017
 * Time: 15:28
 */

namespace W3C\Lib\Classes;

/**
 * Class Arrays
 * @package W3C\Lib\Classes
 */
class Arrays {
    /**
     * @param $array
     *
     * @return array
     */
    public function reverse(array $array): array {
        $reversedArray = [];

        for (\end($array); \key($array) !== null; \prev($array)) {
            $reversedArray[] = \current($array);
        }

        return $reversedArray;
    }

    /**
     * @param array $array
     * @param       $value
     *
     * @return array
     */
    public function removeElementFromArray(array $array, $value): array {
        if (($key = \array_search($value, $array)) !== false) {
            unset($array[$key]);
        }

        return $array;
    }

    /**
     * Check associative array or not
     *
     * @param array $arr
     *
     * @return bool
     */
    public static function isAssociative(array $arr): bool {
        return \array_keys($arr) !== \range(0, \count($arr) - 1);
    }
}