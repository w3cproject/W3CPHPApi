<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 27.02.2017
 * Time: 15:10
 */

namespace W3C\Lib\Classes;

use W3C\Lib\Interfaces\W3CInterface;
use W3C\Lib\Traits\NumbersTrait,
    W3C\Lib\Traits\ContentTrait,
    W3C\Lib\Traits\FilesTrait,
    W3C\Lib\Traits\StringsTrait,
    W3C\Lib\Traits\DatesTrait,
    W3C\Lib\Traits\DBTrait;

/**
 * Class Helpers
 * @package Classes
 */
class Api implements W3CInterface {
    use NumbersTrait,
        ContentTrait,
        FilesTrait,
        StringsTrait,
        DatesTrait,
        DBTrait;

    /**
     * @var string
     */
    private $author = 'W3CProject';
    /**
     * @var string
     */
    private $name = 'W3CProject Helpers';
    /**
     * @var string
     */
    private $version = '1.0';

    /**
     * @return string
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getApiInfo() {
        return "Api: " . $this->getName() . "<br />Author: " . $this->getAuthor() . "<br />Version: " .
            $this->getVersion() . "<br /> Methods: " . "...";
    }

}