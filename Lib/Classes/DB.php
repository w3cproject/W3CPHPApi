<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 27.02.2017
 * Time: 17:15
 */

namespace W3C\Lib\Classes;

/**
 * Class DB
 * @package Classes
 */
class DB {
    /**
     * @var $db \mysqli
     */
    private $db;

    /**
     * @param string $defaultTable
     * @param string $host
     * @param string $user
     * @param string $password
     * @param bool   $status
     *
     * @return \mysqli
     */
    public function connectToDB(
        string $defaultTable,
        string $host,
        string $user,
        string $password,
        bool $status = true
    ): \mysqli {
        $this->db = new \mysqli($host, $user, $password, $defaultTable);

        if ($status === true) {
            if ($this->db->connect_errno) {
                \printf("Connect failed: %s\n", $this->db->connect_error);
                exit();
            } else {
                echo 'DB Connection successful!';
            }
        }

        $this->db->close();

        return $this->db;
    }

    /**
     * @param string $table
     *
     * @return int
     */
    public function showTableNumRows(string $table): int {
        $query = "SELECT * FROM `$table`";

        $result = $this->connectToDB()->Query($query);

        return $result->num_rows;
    }
}