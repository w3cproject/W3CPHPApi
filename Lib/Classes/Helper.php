<?

namespace W3C\Lib\Classes;

class Helper {

    /**
     * Returns Exception if $object not a object type
     * For php >7.2 only
     *
     * @param $object
     *
     * @return object
     */
    public static function acceptObject($object): object {
        return $object;
    }

    /**
     * @param $message
     */
    public static function setMethodAsDeprecated($message) {
        @trigger_error($message);
    }
}