<?
/**
 * Created by PhpStorm.
 * User: W3CProject
 * Date: 27.02.2017
 * Time: 17:16
 */

namespace W3C\Lib\Classes;

/**
 * Class Numbers
 * @package Classes
 */
class Numbers {
    /**
     * @return int
     */
    public function generateNumber(): int {
        return \mt_rand();
    }
}