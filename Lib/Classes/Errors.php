<?

namespace W3C\Lib\Classes;

/**
 * Class Errors
 * @package W3C\Lib\Classes
 */
class Errors {
    /**
     * @param string|NULL $message
     *
     * @return string
     */
    public static function getErrorInitialClassMethodPath(string $message = null): string {
        $err = __CLASS__ . '->' . __FUNCTION__;

        return (!\is_null($message))
            ? $err . ' - ' . $message
            : $err;
    }
}