<?php

namespace W3C\Lib\Generator;

use Exception;

/**
 * Class HtmlToWordException
 */
class HtmlToWordException extends Exception {
}

/**
 * Class HtmlToWord
 * @package W3C\Lib\Generator
 * Convert HTML to MS Word file
 * @author  Harish Chauhan
 * @version 2.0.0
 * @name HtmlToWord
 * Editor: W3CProject
 * What's new:
 * - bring the code for php >= 7.0.1
 * - fix constructor
 * - fix visibility variables/functions
 * - fix syntax/beauty code
 * - add Exceptions
 */
class HtmlToWord {
    /**
     * @var string
     */
    private $docFile;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $htmlHead;

    /**
     * @var string
     */
    private $htmlBody;

    /**
     * HtmlToWord constructor.
     */
    function __construct() {
        $this->title    = 'Untitled Document';
        $this->htmlHead = '';
        $this->htmlBody = '';
    }

    /**
     * @param string $docFile
     */
    public function setDocFileName(string $docFile) {
        $this->docFile = $docFile;

        if (!preg_match("/\.doc$/i", $this->docFile)) {
            $this->docFile .= ".doc";
        }
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getHeader(): string {
        $return = <<<EOH
			 <html xmlns:v="urn:schemas-microsoft-com:vml"
			xmlns:o="urn:schemas-microsoft-com:office:office"
			xmlns:w="urn:schemas-microsoft-com:office:word"
			xmlns="http://www.w3.org/TR/REC-html40">
			
			<head>
			<meta http-equiv=Content-Type content="text/html; charset=utf-8">
			<meta name=ProgId content=Word.Document>
			<meta name=Generator content="Microsoft Word 9">
			<meta name=Originator content="Microsoft Word 9">
			<!--[if !mso]>
			<style>
			v\:* {behavior:url(#default#VML);}
			o\:* {behavior:url(#default#VML);}
			w\:* {behavior:url(#default#VML);}
			.shape {behavior:url(#default#VML);}
			</style>
			<![endif]-->
			<title>$this->title</title>
			<!--[if gte mso 9]><xml>
			 <w:WordDocument>
			  <w:View>Print</w:View>
			  <w:DoNotHyphenateCaps/>
			  <w:PunctuationKerning/>
			  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
			  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
			 </w:WordDocument>
			</xml><![endif]-->
			<style>
			<!--
			 /* Font Definitions */
			@font-face
				{font-family:Verdana;
				panose-1:2 11 6 4 3 5 4 4 2 4;
				mso-font-charset:0;
				mso-generic-font-family:swiss;
				mso-font-pitch:variable;
				mso-font-signature:536871559 0 0 0 415 0;}
			 /* Style Definitions */
			p.MsoNormal, li.MsoNormal, div.MsoNormal
				{mso-style-parent:"";
				margin:0in;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:7.5pt;
			        mso-bidi-font-size:8.0pt;
				font-family:"Verdana";
				mso-fareast-font-family:"Verdana";}
			p.small
				{mso-style-parent:"";
				margin:0in;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:1.0pt;
			        mso-bidi-font-size:1.0pt;
				font-family:"Verdana";
				mso-fareast-font-family:"Verdana";}
			@page Section1
				{size:8.5in 11.0in;
				margin:1.0in 1.25in 1.0in 1.25in;
				mso-header-margin:.5in;
				mso-footer-margin:.5in;
				mso-paper-source:0;}
			div.Section1
				{page:Section1;}
			-->
			</style>
			<!--[if gte mso 9]><xml>
			 <o:shapedefaults v:ext="edit" spidmax="1032">
			  <o:colormenu v:ext="edit" strokecolor="none"/>
			 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
			 <o:shapelayout v:ext="edit">
			  <o:idmap v:ext="edit" data="1"/>
			 </o:shapelayout></xml><![endif]-->
			 $this->htmlHead
			</head>
			<body>
EOH;

        return $return;
    }

    /**
     * @return string
     */
    public function getFooter(): string {
        return '</body></html>';
    }

    /**
     * @param string $url
     * @param string $file
     * @param bool   $download
     *
     * @throws HtmlToWordException
     */
    public function createDocFromURL(string $url, string $file, bool $download = false) {
        if (!preg_match("/^http:/", $url)) {
            $url = "http://" . $url;
        }

        $html = @file_get_contents($url);

        $this->createDoc($html, $file, $download);
    }

    /**
     * @param string $html
     * @param string $file
     * @param bool   $download
     *
     * @throws HtmlToWordException
     */
    public function createDoc(string $html, string $file, bool $download = false) {
        if (is_file($html)) {
            $html = @file_get_contents($html);
        }

        $this->parseHtml($html);
        $this->setDocFileName($file);

        $doc = $this->getHeader();
        $doc .= $this->htmlBody;
        $doc .= $this->getFooter();

        if ($download) {
            @header("Cache-Control: ");// leave blank to avoid IE errors
            @header("Pragma: ");// leave blank to avoid IE errors
            @header("Content-type: application/octet-stream");
            @header("Content-Disposition: attachment; filename=\"$this->docFile\"");

            echo $doc;
        } else {
            $this->writeFile($this->docFile, $doc);
        }
    }

    /**
     * @param string $html
     */
    public function parseHtml(string $html) {
        $html = preg_replace("/<!DOCTYPE((.|\n)*?)>/ims", "", $html);
        $html = preg_replace("/<script((.|\n)*?)>((.|\n)*?)<\/script>/ims", "", $html);

        preg_match("/<head>((.|\n)*?)<\/head>/ims", $html, $matches);

        $head = $matches[1];

        preg_match("/<title>((.|\n)*?)<\/title>/ims", $head, $matches);

        $this->title = $matches[1];

        $html = preg_replace("/<head>((.|\n)*?)<\/head>/ims", "", $html);

        $head = preg_replace("/<title>((.|\n)*?)<\/title>/ims", "", $head);
        $head = preg_replace("/<\/?head>/ims", "", $head);

        $html = preg_replace("/<\/?body((.|\n)*?)>/ims", "", $html);

        $this->htmlHead = $head;
        $this->htmlBody = $html;
    }

    /**
     * @param string $file
     * @param string $content
     * @param string $mode
     *
     * @throws HtmlToWordException
     */
    public function writeFile(string $file, string $content, string $mode = "w") {
        $fp = @fopen($file, $mode);

        if (!is_resource($fp)) {
            throw new HtmlToWordException('File is not resource!');
        }

        fwrite($fp, $content);
        fclose($fp);
    }
}